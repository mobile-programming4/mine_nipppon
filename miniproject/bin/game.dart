import 'dart:io';
import 'board.dart';
import 'Mines.dart';

class Game {
  var side = 0; // side length of the board
  var countMines; // number of countMines on the board
  var table = [];
  Board board = Board(0);
  Mines mines = Mines();

  void chooseDifficultyLevel() {
    /*
    --> BEGINNER = 9 * 9 Cells and 10 Mines
    --> INTERMEDIATE = 16 * 16 Cells and 40 Mines
    --> ADVANCED = 24 * 24 Cells and 99 Mines
    */

    print("Enter the Difficulty Level");
    print("Press 0 for BEGINNER (9 * 9 Cells and 10 Mines)");
    print("Press 1 for INTERMEDIATE (16 * 16 Cells and 40 Mines)");
    print("Press 2 for ADVANCED (24 * 24 Cells and 99 Mines) ");
    stdout.write("Plese choosen Level : ");
    String level = stdin.readLineSync()!;
    if (level == "0") {
      side = 9;
      countMines = 10;
    }
    if (level == "1") {
      side = 16;
      countMines = 40;
    }

    if (level == "2") {
      side = 24;
      countMines = 99;
    }

    if (level == "5"){
      side = 4;
      countMines = 5;
    }
    board.setSide(side);
    board.createBoard(countMines);
    board.printBoard();
  }

  void makemove() {
    print("Enter your move, (row,column) --> ");
    int row = int.parse(stdin.readLineSync()!);
    int col = int.parse(stdin.readLineSync()!);
    board.checkMines(row, col);
  }
}
