import 'dart:math';

import 'Board.dart';
import 'board.dart';

class Mines {
  var row;
  var col;
  var maxMines;
  var xMines = [];
  var yMines = [];

  Mines() {}

  void setMaxMines(int maxMines) {
    this.maxMines = maxMines;
  }

  int getMaxMines() {
    return maxMines;
  }

  void randomMines(int side) {
    final random = new Random();
    for (var i = 0; i < maxMines; i++) {
      var random_x = Random().nextInt(side);
      var random_y = Random().nextInt(side);
      xMines.add(random_x);
      yMines.add(random_y);
    }
    while (checkDuplicateMines(side)){
      checkDuplicateMines(side);
    }
  }

  bool checkDuplicateMines(int side) {
    for (int i = 0; i < maxMines - 1; i++) {
      for (int j = i + 1; j < maxMines; j++) {
        if (xMines[i] == xMines[j] && yMines[i] == yMines[j]) {
          var random_x = Random().nextInt(side);
          var random_y = Random().nextInt(side);
          xMines[j] = random_x;
          yMines[j] = random_y;
          return true;
        }
      }
    }
    return false;
  }

  List getXMines() {
    return xMines;
  }

  List getYMines() {
    return yMines;
  }
}
